// @flow
import React, { Component } from 'react';

type Props = {};

export default class Login extends Component<Props> {
  props: Props;

  render() {
    return (
      <div
        style={{
          background: '#eaeaea',
          height: '100vh',
          width: '100vw'
        }}
      >
        <h1>Login page</h1>
      </div>
    );
  }
}
