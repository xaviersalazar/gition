import type { Dispatch as ReduxDispatch, Store as ReduxStore } from 'redux';

export type state = {};

export type Action = {
  +type: string
};

export type GetState = () => state;

export type Dispatch = ReduxDispatch<Action>;

export type Store = ReduxStore<GetState, Action>;
